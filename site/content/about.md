---
title: "About"
date: 2023-08-10T09:22:32-07:00
draft: false
---

I'm a backend software engineer constantly breaking things to fix them. I've spent most of my developer time using Java,
but I'm really getting into Go.

Here is an overview of my professional history
in [resume](https://f004.backblazeb2.com/file/kurczynski-resume/brodie-kurczynski-resume.pdf) form.

### Website

Powered by [Hugo](http://gohugo.io)

Theme made with &#10084; by [Djordje Atlialp](https://github.com/rhazdon)
