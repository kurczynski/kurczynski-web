FROM alpine:3.20 AS builder

WORKDIR /tmp/build/site
RUN apk add \
    hugo=~0.125 \
    go=~1.22 \
    git \
    && \
    rm -rf /var/cache/apk
COPY site .

RUN hugo mod get -u
RUN hugo --config config/hugo.yaml

FROM nginxinc/nginx-unprivileged:1.27-alpine AS prod

WORKDIR /usr/share/nginx/html
COPY --from=builder /tmp/build/site/public .
