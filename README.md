# kurczynski-web

My personal website: [brodie.kurczynski.org](https://brodie.kurczynski.org).

# Create content

To create a new post, go to the [site](./site) directory and run:

```shell
hugo new posts/some-post.md
```

# Run

In the project root, run:

```shell
skaffold dev
```

# Deploy

See the [Helm chart repo](https://gitlab.com/kurczynski/kurczynski-web-chart#deploy-website).